PImage robotPic;
PImage bgPic;

int healthValue;
int moveDirection;

float roboTopLeft_X;
float roboTopLeft_Y;
float robotMoveSpeed;

void setup(){
  size(640, 480);
  robotPic = loadImage("robot.png");
  bgPic = loadImage("background.png");
  resetGame();
}

void resetGame(){
  healthValue = 10;
  roboTopLeft_X = 150.0;
  roboTopLeft_Y = 150.0;
  moveDirection = 0;  
}


void crosshairAtMouse(){
  stroke(255,0,0);
  noFill();
  ellipse(mouseX, mouseY, 40, 40);
  line(mouseX-30, mouseY, mouseX+30, mouseY);
  line(mouseX, mouseY-30, mouseX, mouseY+30);
}

void redrawBackground(){
  for(int xPos = 0; xPos < width; xPos += bgPic.width){
    for(int yPos = 0; yPos < width; yPos += bgPic.width){
      image(bgPic, xPos, yPos);
    }
  }
}

void showHealth(){
  textAlign(LEFT);
  text("Health: "+ healthValue,4,13);
}

Boolean gameWonByPlayer(){
  return(healthValue <=0);
}

Boolean gameWonByRobot(){
  return (roboTopLeft_X + robotPic.width < 0 ||
     roboTopLeft_Y + robotPic.height < 0 ||
     roboTopLeft_X > width  ||
     roboTopLeft_Y > height);
}


void mouseReleased(){
  if(gameWonByPlayer() || gameWonByRobot()) {
    resetGame();
    return;
  }
  if(mouseX > roboTopLeft_X && 
     mouseY > roboTopLeft_Y &&
     mouseX < roboTopLeft_X + robotPic.width && 
     mouseY < roboTopLeft_Y + robotPic.height ){
       healthValue--;
       moveDirection = int(random(3));
       //moveDirection++;
       if(moveDirection > 3){
         moveDirection = 0;
       }
  }
}

void drawAndMoveRobot(){
  if(gameWonByPlayer()){
    return;
  }
  
  robotMoveSpeed = (10-healthValue)*0.7;
  switch(moveDirection){
    case 0:
      roboTopLeft_X += robotMoveSpeed;
      break;
    case 1:
      roboTopLeft_Y += robotMoveSpeed;
      break;
    case 2:
      roboTopLeft_X -= robotMoveSpeed;    
      break;
    case 3:
      roboTopLeft_Y -= robotMoveSpeed;
      break;
  }
  image(robotPic, roboTopLeft_X, roboTopLeft_Y);
}


void draw(){
  redrawBackground();
  crosshairAtMouse();
  showHealth();
  drawAndMoveRobot();
  
  if(roboTopLeft_X + robotPic.width < 0 ||
     roboTopLeft_Y + robotPic.height < 0 ||
     roboTopLeft_X > width  ||
     roboTopLeft_Y > height){
       textAlign(CENTER);
       text("The robot escaped :(", width/2 , height/2);
       text("Click to play again", width/2 , height/2 + 15);
     }
   
  if(gameWonByPlayer()){
    textAlign(CENTER);
    text("You stopped the robot! :)", width/2, height/2);
    text("Click to play again", width/2, height/2 +15);
  } 
}




