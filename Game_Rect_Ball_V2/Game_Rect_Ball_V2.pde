int h=50, l=10;
float x, y;
float speedX, speedY;
float _x, _y;
float _speedX, _speedY;
int score;
boolean lost = false;

void setup() {
  size(400, 400);
  smooth();
  frameRate(30);
  initialize();
}

void ball() {  
  x += speedX;
  y += speedY; 
  fill(0, 0, 255);
  ellipse(x, y, 20, 20);
}

void _ball() {
  _x += _speedX;
  _y += _speedY; 
  fill(0, 255, 0);
  ellipse(_x, _y, 20, 20);
}

void initialize() {
  x = 50;
  y = 30;
  speedX = 5;
  speedY = 1.8;
  _x = 50;
  _y = 30;
  _speedX = 1.8;
  _speedY = 5;
  score = 0;
  backGround();
  lost = false;
}

void bounce() {
  if ((x > (width-(l*2) - 10)) && (y <= mouseY + h/2) && (y >= mouseY - h/2)) {
    speedX = speedX * -1;
    score = score+1;
  } 
  if ((x < l*2 + 10) && (y <= mouseY + h/2) && (y >= mouseY - h/2) ) {
    speedX = speedX * -1;    
    score = score+1;
  }
  if ((y > (height-(l*2) - 10)) && (x <= mouseX + h/2) && (x >= mouseX - h/2)) {
    speedY = speedY * -1;
    score = score+1;
  }
  if ((y < 20 + 10) && (x <= mouseX + h/2) && (x >= mouseX - h/2)) {
    speedY = speedY * -1;    
    score = score+1;
  }
  if ((_x > (width-(l*2) - 10)) && (_y <= mouseY + h/2) && (_y >= mouseY - h/2)) {
    _speedX = _speedX * -1;
    score = score+1;
  } 
  if ((_x < l*2 + 10) && (_y <= mouseY + h/2) && (_y >= mouseY - h/2) ) {
    _speedX = _speedX * -1;    
    score = score+1;
  }
  if ((_y > (height-(l*2) - 10)) && (_x <= mouseX + h/2) && (_x >= mouseX - h/2)) {
    _speedY = _speedY * -1;
    score = score+1;
  }
  if ((_y < 20 + 10) && (_x <= mouseX + h/2) && (_x >= mouseX - h/2)) {
    _speedY = _speedY * -1;    
    score = score+1;
  }
}

void backGround() {
  background(255, 155, 0);
}

void lose() {
  speedX = 0;
  speedY = 0;
  _speedX = 0;
  _speedY = 0; 
  backGround();
  fill(0);
  text("Game Over", width/2 - 30, height/2);
  text("Click to play again", width/2 - 48, height/2 + 30);
}

void win() {
  speedX = 0;
  speedY = 0;
  _speedX = 0;
  _speedY = 0; 
  backGround();
  fill(0);
  text("You Win", width/2 - 30, height/2);
  text("Click to play again", width/2 - 48, height/2 + 30);
}

void runGame() {
  fill(0);
  text("Score: " + score, width/2 - 20, height/2);
  rect(width-(l*2), mouseY - h/2, l, h);
  rect(l, mouseY - h/2, l, h);
  rect(mouseX - h/2, l, h, l);
  rect(mouseX - h/2, height-(l*2), h, l);
}

void conditions() {
  if ((x > width - 15) || (x < 15) || (y > height - 15) || (_y < 15) || (_x > width - 15) || (_x < 15) || (_y > height - 15) || (_y < 15)) {
    lose();
    lost = true;
  }
  if (!lost) {
    if (score >= 10) {
      _ball();
    }
    if (score >= 20) {
      win();
    }
  }
}

void mouseClicked() {
  initialize();
}

void draw() {
  backGround();
  ball();
  bounce();
  runGame();
  conditions();
}

