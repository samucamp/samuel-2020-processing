//DECLARE  Class ClassINSTANCE
Ball myBall;
Ball mySecondBall;

void setup(){
  size(400, 400);
  smooth();
  
  //INITIALIZE
  myBall = new Ball(200, 200, 1, 0.5);
  mySecondBall = new Ball(100, 100, 3, 0.5);
}

void draw(){
  background(0);
  
  //CALL FUNCTIONALITY
  myBall.run();
  mySecondBall.run();
}
