int r = 0, g = 0, b = 0;

void setup(){
  size(500, 500);
}

void updateBackground(){
  background(g,b,r);
}

void updateEllipse(){
  fill(r,g,b);
  ellipse(mouseX, mouseY, 50, 50);
  r = mouseX;
  g = mouseY;
  b = 255 - (mouseX + mouseY);
}

void draw(){
  updateBackground();
  updateEllipse();
}
