# SAMUEL 2020 - Processing

Programas aleatórios feitos em Processing.

<br>

# Conteúdos
1. [Instalando Processing no UBUNTU](#processing)
2. [Desinstalando](#remove) 
3. [Referências](#ref)

<br>

# Instalando Processing no UBUNTU
<a name="processing"></a>

Baixe o arquivo .tgz de [Processing](https://processing.org/download/)

Extraia o arquivo,
navegue para dentro da pasta pelo terminal,
e execute o comando abaixo.

```bash
sudo ./install.sh
```

Serão criados atalhos e ícone de aplicativo para o arquivo <code>processing</code>.

<br>

# Desinstalando
<a name="remove"></a>

Navegue para dentro da pasta do programa pelo terminal,
e execute o comando abaixo.

```bash
sudo ./uninstall.sh
```

Exclua a pasta.

<br>

# Referências
<a name="ref"></a>

> https://processing.org/download/

