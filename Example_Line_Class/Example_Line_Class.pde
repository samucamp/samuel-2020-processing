// Declare and construct two objects (h1, h2) from the class HLine 
HLine h1 = new HLine(0, 2.0); 
HLine h2 = new HLine(0, 2.5); 
HLine h3 = new HLine(0, 5); 
HLine h4 = new HLine(0, 20); 

void setup() 
{
  size(200, 200);
  frameRate(30);
}

void draw() { 
  background(204);
  h1.update(); 
  h2.update();  
  h3.update();
  h4.update();
} 
 
class HLine { 
  float ypos, speed; 
  HLine (float y, float s) {  
    ypos = y; 
    speed = s; 
  } 
  void update() { 
    ypos += speed; 
    if (ypos > height) { 
      ypos = 0; 
    } 
    line(0, ypos, width, ypos); 
  } 
}
