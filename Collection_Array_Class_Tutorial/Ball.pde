class Ball {
  //GLOBAL VARIABLES
  float x = 0;
  float y = 0;
  float speedX = 0;
  float speedY = 0;
  
  int widthBall = 20;
  
  //CONSTRUCTOR
  Ball(float _x, float _y, float _speedX, float _speedY){
    x = _x;
    y = _y;
    speedX = _speedX;
    speedY = _speedY;
  }
  
  //FUNCTIONS  
  
  void run(){
    display();
    move();
    bounce();
    gravity();
  }
  
  void gravity(){
    speedY += 0.6;
  }
  
  void move (){
    x += speedX;
    y += speedY;
  }
  
  void display(){
    ellipse(x, y, widthBall, widthBall);
  }
  
  void bounce(){
    if (x > width - (widthBall/2)){
      speedX = speedX * -1;
    }
    
    if (x < (widthBall/2)){
      speedX = speedX * -1;
    }
    
    if (y > height - (widthBall/2)){
      speedY = speedY * -1;
    }
    
    if (y < (widthBall/2)){
      speedY = speedY * -1;
    }
  }
}
