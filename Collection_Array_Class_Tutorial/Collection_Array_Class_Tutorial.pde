//DECLARE  Class ClassINSTANCE
Ball [] ballCollection = new Ball [50];

void setup(){
  size(600, 600);
  frameRate(30);
  smooth();
  
  //INITIALIZE //ballCollection.length = 20, what I put in the array Ball [20]
  for (int i = 0; i < ballCollection.length; i ++){
    ballCollection[i] = new Ball(random(0,width), random(0,height/2), 1, 0.5);
  }
}

void draw(){
  background(0);
  
  //CALL FUNCTIONALITY
  for (int i = 0; i < ballCollection.length; i ++){
    ballCollection[i].run();
  }
}
