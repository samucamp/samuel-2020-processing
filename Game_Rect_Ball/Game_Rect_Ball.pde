float x=50, y=30;
int h=50, l=10;
float speedX = 3, speedY = 0.8;

void setup() {
  size(400, 400);
  smooth();
  frameRate(100);
}

void bounce() {
  if ((x > (width-(l*2) - 10)) && (y <= mouseY + h/2) && (y >= mouseY - h/2)) {
    speedX = speedX * -1;
  } 
  if ((x < l*2 + 10) && (y <= mouseY + h/2) && (y >= mouseY - h/2) ){
    speedX = speedX * -1;    
  }
  if (y > (height-20 - 10)) {
    speedY = speedY * -1;
  }
  if (y < 20 + 10){
    speedY = speedY * -1;    
  }
}

void ball() {  
  x += speedX;
  y += speedY; 
  ellipse(x, y, 20, 20);
}

void backGround() {
  background(255, 155, 0);
}

void lose(){
 if (x > width){
   speedX = 0;
   speedY = 0;
   text("You Lose.", width/2 - 30, height/2);
   text("Click to play again.", width/2 - 55, height/2 + 30);
 }else if(x < 0){
   speedX = 0;
   speedY = 0;
   text("You Lose.", width/2 - 30, height/2);
   text("Click to play again.", width/2 - 55, height/2 + 30);
 }
}

void mouseClicked(){
    x=50;
    y=30;
    speedX = 3;
    speedY = 0.8;
    backGround();  
}

void draw() {
  backGround();
  ball();
  bounce();
  fill(0);
  rect(width-(l*2), mouseY - h/2, l, h);
  rect(l, mouseY - h/2, l, h);
  rect(0,height-20, width, 20);
  rect(0,0, width, 20);
  lose();
}

