Name h1 = new Name(5, 10, 100, 50); 
Name h2 = new Name(3, 9, 50, 50);
Name h3 = new Name(3, 1, 20, 20);
Name h4 = new Name(9, 3, 40, 40);
Name h5 = new Name(4, 8, 70, 70);
Name h6 = new Name(2, 4, 60, 60); 
Name h7 = new Name(8, 4, 50, 50);
Name h8 = new Name(4, 2, 20, 20);
Name h9 = new Name(1, 11, 40, 40);
Name h10 = new Name(11, 1, 70, 70);

int margin = 10;

void draw() { 
  
  background(255);
  fill(0,180,0);
  rect(margin, margin, width - 2*margin, height- 2*margin);
  h1.update(); 
  h2.update();
  h3.update();
  h4.update();
  h5.update();
  h6.update(); 
  h7.update();
  h8.update();
  h9.update();
  h10.update();
} 

void setup() {
  size(400, 400);
  frameRate(30);
}

class Name { 

  int ballCos, ballSen, dx, dy;
  int moveX = ballCos/2;
  int moveY = ballSen/2;
  boolean auxMoveX = false;
  boolean auxMoveY = false;
  Name (int a, int b, int c, int d) {  
    ballCos = c; 
    ballSen = d; 
    dx = a;
    dy = b;
  } 

  void update() { 
    fill(100, 0, 255);
    ellipse(moveX, moveY, ballCos, ballSen);
    if (moveX <= ((ballCos/2) + margin)) {  
      auxMoveX = false;
    }
    if (moveX >= (width - (ballCos/2) - margin)) {  
      auxMoveX = true;
    }
    if (auxMoveX) {  
      moveX -= dx;
    } else {  
      moveX += dx;
    }  
    if (moveY <= ((ballSen/2) + margin)) {  
      auxMoveY = false;
    }
    if (moveY >= (height - (ballSen/2) - margin)) {  
      auxMoveY = true;
    }
    if (auxMoveY) {  
      moveY -= dy;
    } else {  
      moveY += dy;
    }
  }
} 
