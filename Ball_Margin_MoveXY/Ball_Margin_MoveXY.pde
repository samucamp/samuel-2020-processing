int margin = 10;
int ballCos = 50;
int ballSen = 50;
int dx = 2;
int dy = 6;

int moveX = margin + ballCos/2;
int moveY = margin + ballSen/2;
boolean auxMoveX = false;
boolean auxMoveY = false;

void setup() {  
  size(400, 400); 
}

void backGround(){
  background(255);
  noStroke();
  fill(moveX,moveY,0);
  rect(margin, margin, width - (2*margin), height - (2*margin));
}

void objectBall(){
  fill(0,0,255);
  ellipse(moveX, moveY, ballCos, ballSen);
}

void ballMoveX(){
  if (moveX <= (margin + (ballCos/2))){  auxMoveX = false;  }
  if (moveX >= (width - margin - (ballCos/2))){  auxMoveX = true;  }
  if (auxMoveX){  moveX -= dx;  }else{  moveX += dx;  }
}

void ballMoveY(){
  if (moveY <= (margin + (ballSen/2))){  auxMoveY = false;  }
  if (moveY >= (height - margin - (ballSen/2))){  auxMoveY = true;  }
  if (auxMoveY){  moveY -= dy;  }else{  moveY += dy;  }
}

void draw() {
  backGround();
  objectBall();
  ballMoveX();
  ballMoveY(); 
}
