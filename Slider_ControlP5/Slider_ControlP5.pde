import controlP5.*;

// 1 DECLARE 
ControlP5  jControl;

int bColor = 0;

void setup(){
  size(400, 400);
  
// 2 INITIALIZE
  jControl = new ControlP5(this);

//("Name", from value, to value, initial value, x, y, width, height); 
  Slider s = jControl.addSlider("bColor", 0, 255, 100, 10, 10, 200, 30);  
}

void draw(){
  background(bColor);
}
  
